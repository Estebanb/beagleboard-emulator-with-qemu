Que estoy haciendo?
1) Voy a intentar generar una imagen de docker con todo lo necesario para correr qemu emulando la beagle bone. (OK)
2) Voy a correr qemu y a hacer que explote todo. (OK) 
3) voy a commitear la imagen de docker con todo lo necesario. (OK)
4) voy a googlear todo lo que pueda sobre el primer error que me salga.
5) voy a preguntar a phil como meterme en la lista de mail para preguntar cosas.

link: https://www.cnx-software.com/2011/09/26/beagleboard-emulator-in-ubuntu-with-qemu/

Objetivo final: Hacer correr la beaglebone y publicarlo open source.

Fue necesario instalar en el host:
sudo apt install qemu-user-static           

La imagen se buildea con:
sudo docker build -t prueba . 

Se corre con:
sudo docker run --privileged -it prueba /bin/bash
