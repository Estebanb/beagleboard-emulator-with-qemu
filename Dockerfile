FROM debian:sid
RUN apt-get update
RUN apt-get install linaro-image-tools wget qemu-system -y
RUN wget http://releases.linaro.org/platform/linaro-n/nano/11.08/nano-n-tar-20110823-1.tar.gz
RUN wget http://releases.linaro.org/platform/linaro-n/nano/11.08/hwpack_linaro-omap3_20110823-0_armel_supported.tar.gz
